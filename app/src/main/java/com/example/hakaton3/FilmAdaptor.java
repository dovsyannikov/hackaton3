package com.example.hakaton3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hakaton3.models.RequestModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FilmAdaptor extends RecyclerView.Adapter<FilmAdaptor.Holder> {

    Context context;
    List<RequestModel.Film> movies;

    public FilmAdaptor(Context context, List<RequestModel.Film> movies) {
        this.context = context;
        this.movies = movies;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.film_main, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Glide.with(context).load(movies.get(position).getPoster_path()).into(holder.imageView);
        holder.textView.setText(movies.get(position).getTitle());

    }
    @Override
    public int getItemCount() {
        return movies.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView.findViewById(R.id.imageView);
            textView.findViewById(R.id.textView);
        }
    }

}
