package com.example.hakaton3.models;

import java.util.List;

public class RequestModel1 {
    List<RequestModel1.Film> results;

    public class Film{
        private String poster_path;
        private String title;

        public String getPoster_path() {
            return poster_path;
        }

        public String getTitle() {
            return title;
        }
    }

    public List<RequestModel1.Film> getResults() {
        return results;
    }
}
