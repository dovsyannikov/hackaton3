package com.example.hakaton3.models;




import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Film.class}, version = 1)
public abstract class MyDataBase extends RoomDatabase {
    public abstract FilmDao1 getFilmDao1();
}

