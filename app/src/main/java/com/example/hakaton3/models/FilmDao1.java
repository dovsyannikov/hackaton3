package com.example.hakaton3.models;

import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

public abstract class FilmDao1 {
    @Insert
    public abstract void insertAll(List<RequestModel.Film> movies);



    @Query(("SELECT * FROM Film"))
    public abstract Flowable<List<Film>> selectAll();


    @Query("DELETE FROM Film")
    public abstract void removeAll();

    public void updateAll(List<RequestModel.Film> movies) {
        removeAll();
        selectAll();

    }
}
