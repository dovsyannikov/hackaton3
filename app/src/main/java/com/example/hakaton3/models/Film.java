package com.example.hakaton3.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Film {

    public Film(){

    }

    public Film(int id, String img_src){
        this.img_src = img_src;
        this.id = id;
    }

    @PrimaryKey
    public int id;
    public String img_src;

    public int getId() {
        return id;
    }

    public String getImg_src() {
        return img_src;
    }
}