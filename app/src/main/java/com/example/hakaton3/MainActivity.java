package com.example.hakaton3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hakaton3.ApiService.ApiService;
import com.example.hakaton3.models.RequestModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

RecyclerView recyclerView;


    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView.setLayoutManager(new GridLayoutManager(this,2));







        disposable = ApiService.getPopularFilms()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<RequestModel>() {
                               @Override
                               public void accept(RequestModel requestModel) throws Exception {
                                   Toast.makeText(MainActivity.this, requestModel.getResults().size()+"", Toast.LENGTH_SHORT).show();
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
                               }
                           });

    }



}
